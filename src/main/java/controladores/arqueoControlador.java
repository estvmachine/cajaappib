/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.ArqueoCaja;
import org.bson.types.ObjectId;

public class arqueoControlador {
    
    private final DBCollection arqueoCajaCollection;
    private MongoClient cliente;
    private DB database;
    
    public arqueoControlador() {
          
        try {
        // Creo una instancia de cliente de MongoDB
            cliente = new MongoClient(new ServerAddress("localhost", 27017));
        } catch (UnknownHostException ex) {
            Logger.getLogger(ex.toString());
        }
          
        //Creo conexion con base de datos
        database = cliente.getDB("AppBasilea");
        arqueoCajaCollection = database.getCollection("arqueos");
         
    }
    
    public boolean agregarArqueo(ArqueoCaja arqueo){
        
        // Inserto el documento en la coleccion de transacciones en el database
        try {
            arqueoCajaCollection.insert(arqueo);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    }
    
    public boolean agregarArqueo(ArqueoCaja arqueo,   
                                    String sumaTotal, 
                                     ArrayList<String> billetesNuevos,
                                     ArrayList<String> billetesNuevosInut,
                                     ArrayList<String> billetesAntiguos,
                                     ArrayList<String> billetesAntiguosInut,
                                     ArrayList<String> monedas){
        // Inserto el documento en la coleccion de transacciones en el database
        try {
            arqueoCajaCollection.insert(arqueo.append("Saldo Arqueo", sumaTotal)
                                              .append("billetesNuevos", billetesNuevos)
                                              .append("billetesNuevosInut", billetesNuevosInut)   
                                              .append("billetesAntiguos", billetesAntiguos)   
                                              .append("billetesAntiguosInut", billetesAntiguosInut)   
                                              .append("monedas", monedas));  
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
        
    
    }
    
    public DBObject ultimoArqueo(String rut_cajero, String dia_contable) {
        
       try{
            DBObject query = QueryBuilder.start("rut_cajero").is(rut_cajero)
                            .and("dia_contable").is(dia_contable)
                            .and("estado").is("Iniciado").get();

            DBObject arqueo = arqueoCajaCollection.findOne(query);
            return arqueo;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    public DBObject actualizarArqueo(String id_arqueo, 
                                     String sumaTotal, 
                                     ArrayList<String> billetesNuevos,
                                     ArrayList<String> billetesNuevosInut,
                                     ArrayList<String> billetesAntiguos,
                                     ArrayList<String> billetesAntiguosInut,
                                     ArrayList<String> monedas){
       
       //Solo modifica el campo, si no existe lo genera.
        arqueoCajaCollection.update(new BasicDBObject("_id", new ObjectId(id_arqueo))
                ,new BasicDBObject("$set", new BasicDBObject("Saldo Arqueo", sumaTotal)
                                              .append("billetesNuevos", billetesNuevos)
                                              .append("billetesNuevosInut", billetesNuevosInut)   
                                              .append("billetesAntiguos", billetesAntiguos)   
                                              .append("billetesAntiguosInut", billetesAntiguosInut)   
                                              .append("monedas", monedas)   
                ));
                
        try{
            DBObject query = QueryBuilder.start("_id").is(id_arqueo).get();

            DBObject arqueo = arqueoCajaCollection.findOne(query);
            return arqueo;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
     }
    
    }
    
    public boolean cambiarEstado(String id_arqueo, String estado){
        
        try{
            arqueoCajaCollection.update(new BasicDBObject("_id", new ObjectId(id_arqueo)),
                    new BasicDBObject("$set", new BasicDBObject("estado", estado)));

            return true;
        }
        catch(Exception e){
            return false;
        }
                
    }
   
    

}