/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.Transaccion;

/**
 *
 * @author STEV
 */
public class reversaControlador {
    
    private final DBCollection transaccionesCollection;
    private MongoClient cliente;
    private DB database;
    
    public reversaControlador() {
          
        try {
        // Creo una instancia de cliente de MongoDB
            cliente = new MongoClient(new ServerAddress("localhost", 27017));
        } catch (UnknownHostException ex) {
            Logger.getLogger(ex.toString());
        }
          
        //Creo conexion con base de datos
        database = cliente.getDB("AppBasilea");
        
        transaccionesCollection = database.getCollection("transacciones");
         
    }
    
    public boolean producirReversa(Transaccion transaccion){
        
        // Inserto el documento en la coleccion de transacciones en el database
        try {
            transaccionesCollection.insert(transaccion);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    }
    
    public boolean producirReversa(String cod_transaccion, String rut_cliente, String rut_cajero, int monto, String cuenta, String estado, Date now){

            //Determino la fecha para guardarla
            if (now != null)
                now = new Date();

            // Creo un documento para agregar a la coleccion.
            DBObject document = new BasicDBObject("cod_transaccion", cod_transaccion)
                    .append("rut_cliente", rut_cliente)
                    .append("rut_cajero", rut_cajero)
                    .append("monto", monto)
                    .append("cuenta", cuenta)
                    .append("estado", estado)
                    .append("create_at", now);

            // Inserto el documento en la coleccion de transacciones en el database
        try {
            transaccionesCollection.insert(document);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    

    }

}