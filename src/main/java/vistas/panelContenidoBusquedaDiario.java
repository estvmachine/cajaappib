/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import com.mongodb.DBObject;
import controladores.transaccionControlador;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 *
 * @author STEV
 */
public class panelContenidoBusquedaDiario extends javax.swing.JFrame {

    
    private transaccionControlador transaccionControl;
    private List<DBObject> listaTransacciones;
    private DBObject cajero;
    private String diaContable;
    
    
    panelContenidoBusquedaDiario(DBObject cajero, String diaContable, String hora_ini, String hora_fin, String estadoTrans_escogida, String tipoTrans_escogida) {
        initComponents();
        this.setLayout(new FlowLayout());
        setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
 
        transaccionControl= new transaccionControlador();
        this.cajero= cajero;
        this.diaContable= diaContable;
                
        //Cargo los combobox con informacion
        this.comboboxUsuario.addItem(cajero.get("_id").toString());
        this.comboboxFecha.addItem(diaContable);
        this.fieldHoraInicio.setText(hora_ini);
        this.fieldHoraFin.setText(hora_fin);
        
        //Obtengo la lista de transacciones que cumplen la busqueda
        
         List<DBObject> listFiltros = new ArrayList<>();
         listFiltros = transaccionControl.addFiltro(listFiltros , "rut_cajero", cajero.get("rut").toString() );
         listFiltros = transaccionControl.addFiltro(listFiltros, "diaContable", diaContable);
         /*listFiltros = transaccionControl.addFiltro(listFiltros, "hora_ini", hora_ini);
         listFiltros = transaccionControl.addFiltro(listFiltros, "hora_fin", hora_fin);
         listFiltros = transaccionControl.addFiltro(listFiltros, "estado", "Aceptada");*/
        listFiltros = transaccionControl.addFiltro(listFiltros, "tipoTrans", tipoTrans_escogida);
         
         listaTransacciones = transaccionControl.filtrar(listFiltros);
         //System.out.println(listFiltros);
         
        
        //Seteo la lista para que se muestre en pantalla
         List<DBObject> tempLista = new ArrayList<>(); 
         ObjectId  id= null;
         DateTime fecha= null;
         Iterator<DBObject> iterator = listaTransacciones.iterator();
        
        
        while(iterator.hasNext()){
                        
            DBObject element = (DBObject)iterator.next();
            
            //Convierto ObjectId al string que representa
            id= (ObjectId) element.get("_id");
            element.put("_id", id.toString() );
            
            //Conversion de mongodb data a Date, y luego a DateTime
            Date tfecha= (Date) element.get("create_at");
            fecha= new DateTime(tfecha);
            element.put("create_at", fecha.toString());
            
            //Agrego el elemento a la lista
            tempLista.add(element);
        } 
        
        
        //Construyo la tabla
         String col[] = {"id", "Grabado", "COD","Cajero","M.Contable","T.Apertura","Dia Contable", "Monto", "Cuenta", "Tipo", "Estado", "Cliente" };
         DefaultTableModel tableModel = new DefaultTableModel(col, 0);    // The 0 argument is number rows.
         
         //Itero la lista temporal para llenar las filas
         
         iterator = tempLista.iterator();
         while(iterator.hasNext()){
             DBObject element = (DBObject)iterator.next();
             
             //Variables que pueden ser nulas
             String monto = asignValue(element, "monto");
             String cuenta = asignValue(element, "cuenta");
             String tipoCuenta = asignValue(element, "tipoCuenta");
             String estado = asignValue(element, "estado");
             String rut_cliente = asignValue(element, "rut_cliente");
             String Modo_Contable= asignValue(element, "Modo_Contable");
             if(Modo_Contable.length()!=0)
                Modo_Contable= Modo_Contable.substring(0,2);
             
             String Tipo_Apertura = asignValue(element, "Tipo_Apertura");
             if(Tipo_Apertura.length() !=0)
                 Tipo_Apertura= Tipo_Apertura.substring(0,2);
             
             String diaContableDB = asignValue(element, "diaContable");
             
             
                        
             String addRowElement[]= { element.get("_id").toString(),
                                element.get("create_at").toString(),
                                element.get("cod_transaccion").toString(),
                                element.get("rut_cajero").toString(),
                                Modo_Contable,
                                Tipo_Apertura,
                                diaContableDB,
                                monto,
                                cuenta,
                                tipoCuenta,
                                estado,
                                rut_cliente
                               };
            tableModel.addRow(addRowElement);
             
         }
        
         this.tablaTransacciones.setModel(tableModel);
         
     
        
           
    }
    
    private String asignValue(DBObject element, String key){
        
        String temp;
        
             try{
                temp = element.get(key).toString();
                return temp;
             }
             catch(Exception e){
                temp = "";
                return temp;
             }
    
    }
    
    panelContenidoBusquedaDiario() {
        initComponents();
        setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this.setLayout(new FlowLayout());
        transaccionControl= new transaccionControlador();
 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonCerrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaTransacciones = new javax.swing.JTable();
        RangoHoraWindow = new javax.swing.JPanel();
        fieldHoraInicio = new javax.swing.JTextField();
        fieldHoraFin = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboboxUsuario = new javax.swing.JComboBox();
        comboboxFecha = new javax.swing.JComboBox();
        botonCerrar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setPreferredSize(new java.awt.Dimension(1200, 600));

        botonCerrar.setText("Volver");
        botonCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarActionPerformed(evt);
            }
        });

        jLabel1.setText("Reporte del Diario Electrónico");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel3.setToolTipText("");

        tablaTransacciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaTransacciones);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
        );

        RangoHoraWindow.setBorder(javax.swing.BorderFactory.createTitledBorder("Rango de Hora"));
        RangoHoraWindow.setToolTipText("");
        RangoHoraWindow.setName(""); // NOI18N

        fieldHoraInicio.setEditable(false);
        fieldHoraInicio.setText("08:00");

        fieldHoraFin.setEditable(false);
        fieldHoraFin.setText("18:00");

        jLabel4.setText("Inicio:");

        jLabel5.setText("Fin:");

        javax.swing.GroupLayout RangoHoraWindowLayout = new javax.swing.GroupLayout(RangoHoraWindow);
        RangoHoraWindow.setLayout(RangoHoraWindowLayout);
        RangoHoraWindowLayout.setHorizontalGroup(
            RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RangoHoraWindowLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(fieldHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addGroup(RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(fieldHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35))
        );
        RangoHoraWindowLayout.setVerticalGroup(
            RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RangoHoraWindowLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(RangoHoraWindowLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fieldHoraInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fieldHoraFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel6.setText("Fecha Contable");

        jLabel7.setText("Selección de Usuarios");

        comboboxUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] {}));
        comboboxUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboboxUsuarioActionPerformed(evt);
            }
        });

        comboboxFecha.setModel(new javax.swing.DefaultComboBoxModel(new String[] {}));

        botonCerrar1.setText("Imprimir");
        botonCerrar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(249, 249, 249)
                        .addComponent(botonCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonCerrar1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(181, 181, 181)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7)
                            .addComponent(comboboxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(comboboxFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)
                        .addComponent(RangoHoraWindow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(352, 352, 352)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(464, Short.MAX_VALUE))
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(4, 4, 4)
                                .addComponent(comboboxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboboxFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(RangoHoraWindow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonCerrar)
                    .addComponent(botonCerrar1))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1200, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrarActionPerformed

    }//GEN-LAST:event_botonCerrarActionPerformed

    private void comboboxUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboboxUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboboxUsuarioActionPerformed

    private void botonCerrar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_botonCerrar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(panelContenidoBusquedaDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(panelContenidoBusquedaDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(panelContenidoBusquedaDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(panelContenidoBusquedaDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new panelContenidoBusquedaDiario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel RangoHoraWindow;
    private javax.swing.JButton botonCerrar;
    private javax.swing.JButton botonCerrar1;
    private javax.swing.JComboBox comboboxFecha;
    private javax.swing.JComboBox comboboxUsuario;
    private javax.swing.JTextField fieldHoraFin;
    private javax.swing.JTextField fieldHoraInicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaTransacciones;
    // End of variables declaration//GEN-END:variables
}
