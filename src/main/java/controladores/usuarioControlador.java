/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import modelos.Usuario;
import sun.misc.BASE64Encoder;

/**
 *
 * @author STEV
 */
public class usuarioControlador {
    
    private final DBCollection usersCollection;
    private Random random = new SecureRandom();

    public usuarioControlador(final DB Database) {
        
        usersCollection = Database.getCollection("users");
        
    }

    // Valido que usuario es unico y lo inserto en la DB

        public boolean addUser(Usuario user) {
    
        try {
            usersCollection.insert(user);
            return true;
        } catch (MongoException.DuplicateKey e) {
            System.out.println("El usuario: " + user.getString("_id")+ " ya se encuentra registrado");
            return false;
        }
    }
    
    
    public boolean addUser(String username, String password) {
        String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));

        BasicDBObject user = new BasicDBObject();

        user.append("_id", username).append("password", passwordHash);

        try {
            usersCollection.insert(user);
            return true;
        } catch (MongoException.DuplicateKey e) {
            System.out.println("El usuario: " + username+ " ya se encuentra registrado");
            return false;
        }
    }
    
    public boolean addUser(String username, String password, String email) {

        String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));

        BasicDBObject user = new BasicDBObject();

        user.append("_id", username).append("password", passwordHash);

        if (email != null && !email.equals("")) {
            // the provided email address
            user.append("email", email);
        }

        try {
            usersCollection.insert(user);
            return true;
        } catch (MongoException.DuplicateKey e) {
            System.out.println("El usuario: " + username+ " ya se encuentra registrado");
            return false;
        }
    }

    public DBObject validateLogin(String username, String password) {
        
        DBObject user;
        user = usersCollection.findOne(new BasicDBObject("_id", username));

        if (user == null) {
            System.out.println("Usuario no registrado");
            return null;
        }

        String hashedAndSalted = user.get("password").toString();

        String salt = hashedAndSalted.split(",")[1];     //Encriptacion a partir de la coma

        if (!hashedAndSalted.equals(makePasswordHash(password, salt))) {
            System.out.println("Contrasenia incorrecta");
            return null;
        }

        return user;
    }


    private String makePasswordHash(String password, String salt) {
        try {
            String saltedAndHashed = password + "," + salt;
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(saltedAndHashed.getBytes());
            BASE64Encoder encoder = new BASE64Encoder();
            byte hashedBytes[] = (new String(digest.digest(), "UTF-8")).getBytes();
            return encoder.encode(hashedBytes) + "," + salt;    //Encriptacion a partir de la coma
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 is not available", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
        }
    }
    
     
    public List<DBObject> listar() {
        
       try{
            DBCursor resultados = usersCollection.find();
            return resultados.toArray();
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }

    
}