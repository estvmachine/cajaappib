/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import com.mongodb.DBObject;
import controladores.cuentaControlador;
import controladores.transaccionControlador;
import java.awt.event.KeyEvent;
import java.util.Date;
import modelos.Transaccion;

/**
 *
 * @author STEV
 */
public class panelDepositoEfectivo_TR0300 extends javax.swing.JPanel {

    private transaccionControlador transaccionControl;
    private cuentaControlador cuentaControl;
    private DBObject cajero;
    private String diaContable;
    
    

    panelDepositoEfectivo_TR0300(DBObject cajero, String diaContable) {
         initComponents();
         this.cajero= cajero;
         this.diaContable = diaContable;
         transaccionControl= new transaccionControlador();
         cuentaControl = new cuentaControlador();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        botonAceptarCCTE = new javax.swing.JButton();
        inputMonto = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        inputNumCuenta = new javax.swing.JTextField();
        inputTipoCuenta = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        Resultados = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        outputRutTitular = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        outputNombreTitular = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        outputRespuesta = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Depósito"));
        jPanel1.setToolTipText("Ingreso CCTE");

        botonAceptarCCTE.setText("Aceptar");
        botonAceptarCCTE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarCCTEActionPerformed(evt);
            }
        });

        inputMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                inputMontoKeyTyped(evt);
            }
        });

        jLabel4.setText("Monto              :");

        jLabel3.setText("N.de cuenta    :");

        inputNumCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                inputNumCuentaKeyTyped(evt);
            }
        });

        inputTipoCuenta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cuenta Corriente", "Cuenta RUT"}));

        jLabel2.setText("Tipo de Cuenta:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(inputMonto)
                    .addComponent(inputNumCuenta)
                    .addComponent(inputTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(botonAceptarCCTE)
                        .addGap(35, 35, 35)))
                .addContainerGap(182, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(inputTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(inputNumCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inputMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonAceptarCCTE)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        Resultados.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados"));
        Resultados.setToolTipText("");
        Resultados.setName("Resultados"); // NOI18N

        jLabel5.setText("RUT del Titular     :");

        outputRutTitular.setEditable(false);

        jLabel6.setText("Nombre del Titular:");

        outputNombreTitular.setEditable(false);
        outputNombreTitular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outputNombreTitularActionPerformed(evt);
            }
        });

        outputRespuesta.setEditable(false);
        outputRespuesta.setColumns(20);
        outputRespuesta.setRows(5);
        jScrollPane1.setViewportView(outputRespuesta);

        jLabel7.setText("Respuesta            :");

        javax.swing.GroupLayout ResultadosLayout = new javax.swing.GroupLayout(Resultados);
        Resultados.setLayout(ResultadosLayout);
        ResultadosLayout.setHorizontalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResultadosLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ResultadosLayout.createSequentialGroup()
                        .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(outputNombreTitular, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                            .addComponent(outputRutTitular, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addGroup(ResultadosLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        ResultadosLayout.setVerticalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResultadosLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(outputRutTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(outputNombreTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(0, 26, Short.MAX_VALUE))
        );

        jLabel1.setText("0300 Depósito en Efectivo a Cuenta Corriente");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Resultados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(152, 152, 152)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Resultados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.getAccessibleContext().setAccessibleName("Ingreso CCTE");
    }// </editor-fold>//GEN-END:initComponents

    private void outputNombreTitularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outputNombreTitularActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_outputNombreTitularActionPerformed

    private void botonAceptarCCTEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarCCTEActionPerformed
        // TODO add your handling code here:
        
        String tipoCuenta= this.inputTipoCuenta.getSelectedItem().toString();
        String numCuenta= this.inputNumCuenta.getText().toString();
        int monto= Integer.parseInt(this.inputMonto.getText().toString());
        Transaccion transaccion= new Transaccion("0300");
        
        DBObject cuenta= cuentaControl.validarCuenta(numCuenta, tipoCuenta);
        
        if(cuenta != null){
            transaccion.append("rut_cliente", cuenta.get("rut_cliente"))
                       .append("rut_cajero", cajero.get("rut"))
                       .append("monto",monto)
                       .append("cuenta", numCuenta)
                       .append("tipoCuenta",tipoCuenta)
                       .append("diaContable", diaContable)
                       .append("estado", "Aceptada");
            
            this.outputNombreTitular.setText( cuenta.get("nombre").toString() +" "+ cuenta.get("apellido").toString() );
            this.outputRutTitular.setText(cuenta.get("rut_cliente").toString());
            this.outputRespuesta.setText("Transacción ha sido un éxito");
            System.out.println(transaccionControl.agregarTransaccion(transaccion));
            System.out.println(cuentaControl.addMonto(monto, numCuenta, tipoCuenta));
        } 
        else{
            this.outputNombreTitular.setText("");
            this.outputRutTitular.setText("");
            this.outputRespuesta.setText("Ha habido un error");
            transaccion.append("rut_cliente", "Error")
                       .append("rut_cajero", cajero.get("rut"))
                       .append("monto",monto)
                       .append("cuenta", numCuenta)
                       .append("tipoCuenta",tipoCuenta)
                       .append("diaContable", diaContable)
                       .append("estado", "Cancelada");
            System.out.println(transaccionControl.agregarTransaccion(transaccion));
        }
            
        
               
    }//GEN-LAST:event_botonAceptarCCTEActionPerformed

    private void inputMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputMontoKeyTyped
        // TODO add your handling code here:
        
        char c = evt.getKeyChar();
        
        if(c<'0' | c> '9' ) evt.consume();
    }//GEN-LAST:event_inputMontoKeyTyped

    private void inputNumCuentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputNumCuentaKeyTyped
        // TODO add your handling code here:
        
        char c = evt.getKeyChar();
        
         if(  (c==(char)KeyEvent.VK_SPACE))  evt.consume();
                
               
    }//GEN-LAST:event_inputNumCuentaKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Resultados;
    private javax.swing.JButton botonAceptarCCTE;
    private javax.swing.JTextField inputMonto;
    private javax.swing.JTextField inputNumCuenta;
    private javax.swing.JComboBox inputTipoCuenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField outputNombreTitular;
    private javax.swing.JTextArea outputRespuesta;
    private javax.swing.JTextField outputRutTitular;
    // End of variables declaration//GEN-END:variables
}
