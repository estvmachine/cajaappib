/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import sun.misc.BASE64Encoder;


/**
 *
 * @author STEV
 */
public class Usuario extends BasicDBObject{
 
    private Random random = new SecureRandom();
    
    
    private Usuario() {
        
    }

    public Usuario(String username, String password) {
        String passwordHash = makePasswordHash(password, Integer.toString(random.nextInt()));
        this.append("_id", username).append("password", passwordHash);

    }

    public void addCuenta(String rut, String numCuenta, String tipoCuenta) {
        this.append("rut",rut)
                 .append("numCuenta", numCuenta)
                 .append("tipoCuenta",tipoCuenta);

    }
        
    private String makePasswordHash(String password, String salt) {
        try {
            String saltedAndHashed = password + "," + salt;
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(saltedAndHashed.getBytes());
            BASE64Encoder encoder = new BASE64Encoder();
            byte hashedBytes[] = (new String(digest.digest(), "UTF-8")).getBytes();
            return encoder.encode(hashedBytes) + "," + salt;    //Encriptacion a partir de la coma
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 is not available", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 unavailable?  Not a chance", e);
        }
    }
    
}
