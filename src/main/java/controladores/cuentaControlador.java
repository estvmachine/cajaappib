/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelos.Cuenta;

public class cuentaControlador {
    
    private final DBCollection cuentaesCollection;
    private MongoClient cliente;
    private DB database;
    
    
    public cuentaControlador() {
         
        try {
        // Creo una instancia de cliente de MongoDB
            cliente = new MongoClient(new ServerAddress("localhost", 27017));
        } catch (UnknownHostException ex) {
            Logger.getLogger(ex.toString());
        }
          
        //Creo conexion con base de datos
        database = cliente.getDB("AppBasilea");
        
        cuentaesCollection = database.getCollection("cuentas");

    }
    
    public boolean agregarCuenta(Cuenta cuenta){
        
           //Determino la fecha para guardarla
            Date now = new Date();
            cuenta.append("create_at", now);
            
          // Inserto el documento en la coleccion de cuentaes en el database
        try {
            cuentaesCollection.insert(cuenta);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    

    }
    public boolean agregarCuenta(String rut_cliente, String num_cuenta,  String tipo_cuenta){

            //Determino la fecha para guardarla
            Date now = new Date();

            // Creo un documento para agregar a la coleccion.
            DBObject document = new BasicDBObject("num_cuenta", num_cuenta)
                    .append("rut_cliente", rut_cliente)
                    .append("monto", "")
                    .append("create_at", now);

            // Inserto el documento en la coleccion de cuentaes en el database
        try {
            cuentaesCollection.insert(document);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    

    }
    
    public DBObject validarCuenta(String num_cuenta, String tipo_cuenta){
        
     try{
        DBObject query = QueryBuilder.start("_id").is(num_cuenta)
                .and("tipo_cuenta").is(tipo_cuenta).get();

        DBObject cuenta = cuentaesCollection.findOne(query);
        System.out.println("Cuenta " +cuenta);
        return cuenta;
     }
     catch(Exception e){
         System.out.println(e);
         return null;
     }
    }
    
    public DBObject addMonto(int monto, String num_cuenta, String tipo_cuenta){
       
       //Solo modifica el campo, si no existe lo genera.
        cuentaesCollection.update(new BasicDBObject("_id", num_cuenta).append("tipo_cuenta", tipo_cuenta)
                ,new BasicDBObject("$inc", new BasicDBObject("monto", monto)));
                
        try{
            DBObject query = QueryBuilder.start("_id").is(num_cuenta)
                            .and("tipo_cuenta").is(tipo_cuenta).get();

            DBObject cuenta = cuentaesCollection.findOne(query);
            return cuenta;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
     }
    
    }
 
}