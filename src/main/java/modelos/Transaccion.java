/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.mongodb.BasicDBObject;
import java.util.Date;

/**
 *
 * @author STEV
 */
public class Transaccion extends BasicDBObject{
        
    /**
     *
     * @param cod_transaccion
     */
    public Transaccion (String cod_transaccion){       
      
        Date now = new Date();
        this.append("cod_transaccion", cod_transaccion)
            .append("create_at", now);
    }
    
    public Transaccion (BasicDBObject transaccion){       
      
        Date now = new Date();
        transaccion.append("create_at", now);
        
    }
    
    
    public Transaccion (String cod_transaccion, String rut_cajero){       
      
        Date now = new Date();
        this.append("cod_transaccion", cod_transaccion)
            .append("rut_cajero", rut_cajero)
            .append("create_at", now);
    }
    
    public Transaccion (String cod_transaccion, String rut_cliente, String rut_cajero, int monto, String cuenta, String tipoCuenta, String estado){       
       
       Date now = new Date();
       this.append("cod_transaccion", cod_transaccion)
                    .append("rut_cliente", rut_cliente)
                    .append("rut_cajero", rut_cajero)
                    .append("monto", monto)
                    .append("estado", "")
                    .append("cuenta", cuenta)
                    .append("tipoCuenta", tipoCuenta);
    }
   
  
   public void estadoTransaccion(int estado){ 
       
       switch(estado){
           
           case(0):
               this.remove("estado");
               this.put("estado", "Aceptada");
               break;
           case(1):
               this.remove("estado");
               this.put("estado", "Pendiente");
                break;
           case(2):
               this.remove("estado");
               this.put("estado", "Enviada");
                break;
           case(3):
               this.remove("estado");
               this.put("estado", "Cancelada");
                break;
           case(4):
               this.remove("estado");
               this.put("estado", "Time Out/ Pendiente");
                break;
           case(5):
               this.remove("estado");
               this.put("estado", "Time Out/ Cancelada");
               break;
           case(6):
               this.remove("estado");
               this.put("estado", "Transaccion rechazada");
               break;
           case(7):
               this.remove("estado");
               this.put("estado", "Requiere Supervisor");
               break;
               
       }
    }
}
