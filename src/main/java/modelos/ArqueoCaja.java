/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.mongodb.BasicDBObject;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author STEV
 */
public class ArqueoCaja extends BasicDBObject{
  
    
    public ArqueoCaja(String rut_cajero, String dia_contable){
        Date now = new Date();
        this.append("create_at", now)
            .append("rut_cajero", rut_cajero)
            .append("dia_contable",dia_contable)
            .append("estado", "Iniciado");     
    }
    
    public ArqueoCaja (BasicDBObject arqueo){       
        Date now = new Date();
        arqueo.append("create_at", now);
       
    } 
    
    /**
     **Por convencion la lista de billetes y monedas es de valores descendentes, con excepcion de 'monedas'
     *    
     *    billetesNuevos={ 20.000,10.000,5.000, 2.000, 1.000}
     *    billetesNuevosInut={ 20.000,10.000,5.000, 2.000, 1.000}
     *    billetesAntiguos={ 20.000,10.000,5.000, 2.000, 1.000}
     *    billetesAntiguosInut={ 20.000,10.000,5.000, 2.000, 1.000, 500}
     *    
     *    En el caso de monedas, se utiliza el formato que se utiliza en las tablas 
     *    monedas={ 500, 50, 5, 100, 10, 1  }
     * 
     **/
    public ArqueoCaja (String[] billetesNuevos, String[] billetesNuevosInut, String[] billetesAntiguos, String[] billetesAntiguosInut,
            String[] Monedas, int sumaTotal, String cod_moneda, String rut_cajero, String dia_contable){  
        
      this.put("billetesNuevos", Arrays.asList(billetesNuevos));
      this.put("billetesNuevosInut", Arrays.asList(billetesNuevosInut));
      this.put("billetesAntiguos", Arrays.asList(billetesAntiguos));
      this.put("billetesAntiguosInut", Arrays.asList(billetesAntiguosInut));
      this.put("Monedas", Arrays.asList(Monedas));
      this.put("Saldo Arqueo", sumaTotal);
      this.put("Codigo Moneda", cod_moneda);
      this.put("rut_cajero", rut_cajero);
      this.put("dia_contable",dia_contable);
      this.put("estado", "Iniciado");
           
      Date now = new Date();
      this.append("create_at", now);
    }
    
   
    
   public void estadoArqueo(int estado){ 
       
       switch(estado){
           
           case(0):
               this.remove("estado");
               this.put("estado", "Iniciado");
               break;
           case(1):
               this.remove("estado");
               this.put("estado", "Finalizado");
                break;
               
       }
    }
}
