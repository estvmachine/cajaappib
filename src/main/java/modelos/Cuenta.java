/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.util.Date;

/**
 *
 * @author STEV
 */
public class Cuenta extends  BasicDBObject{
 
         
    public Cuenta (String rut_cliente, String nombre, String apellido, String num_cuenta, String tipo_cuenta){       
        
       this.append("_id", num_cuenta)
                    .append("rut_cliente", rut_cliente)
                    .append("nombre", nombre)
                    .append("apellido", apellido)
                    .append("monto", 0)
                    .append("estado", "")
                    .append("tipo_cuenta", tipo_cuenta);
    }
    
    public void estadoCuenta(int estado){ 
       
       switch(estado){
           
           case(0):
               this.remove("estado");
               this.put("estado", "Cuenta OK");
               break;
           case(1):
               this.remove("estado");
               this.put("estado", "Cuenta en Observación");
               break;
           case(2):
               this.remove("estado");
               this.put("estado", "Cuenta Cerrada Banco");
                break;
           case(3):
               this.remove("estado");
               this.put("estado", "Cuenta Cancelada");
                break;
           case(4):
               this.remove("estado");
               this.put("estado", "Cuenta No Vigente");
                break;
         
       }
    }
    
    
}
