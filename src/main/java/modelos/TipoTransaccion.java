/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import com.mongodb.BasicDBObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author STEV
 */
public class TipoTransaccion extends BasicDBObject{
    
    static ArrayList<String> opsEgreso = new ArrayList<>( Arrays.asList("0500")); 
    static ArrayList<String> opsIngreso = new ArrayList<>( Arrays.asList("0480" , "0300")); 
    static ArrayList<String> opsCaja = new ArrayList<>( Arrays.asList("9111", "9222"));
    static ArrayList<String> opsReversa = new ArrayList<>();
    static ArrayList<String> opsAdmin = new ArrayList<>();
    static ArrayList<String> opsOtros = new ArrayList<>(Arrays.asList("9111", "9222"));
    static ArrayList<String> listOpsPermitidas = new ArrayList<>(Arrays.asList("egreso", "ingreso", "reversa","opCaja", "opAdmin", "otros"));

            
    public TipoTransaccion (String cod_transaccion, String tipo_transaccion){       
        this.append("tipo_transaccion", tipo_transaccion);
        this.append("cod_transaccion" , cod_transaccion );        
    }
    
    public String defineTypeTransaccion(String cod_transaccion){ 
       if( opsEgreso.indexOf(cod_transaccion) != -1){
           return "egreso";
       }
       
       else if( opsIngreso.indexOf(cod_transaccion) != -1){
           return "ingreso";
       }
       
       else if(opsReversa.indexOf(cod_transaccion) != -1){
           return "reversa";
       }
       
       else if(opsCaja.indexOf(cod_transaccion) != -1){
           return "opCaja";
       }
       
       else if(opsAdmin.indexOf(cod_transaccion) != -1){
           return "opAdmin";
       }
       else{
           return "no definido";
       }
       
    }
    public static boolean isAceptableList(String tipo_transaccion){
        if(listOpsPermitidas.indexOf(tipo_transaccion) == -1){
            return false;
        }
        else{
            return true;
        }
    }
    public static ArrayList<String> getListTransaccionByType(String tipo_transaccion){
        
        if("egreso".equals(tipo_transaccion)){
            return opsEgreso;
        }
        else if("ingreso".equals(tipo_transaccion)){
            return opsIngreso;
        }
        
        else if("reversa".equals(tipo_transaccion)){
            return opsReversa;
        }
        
        else if("opCaja".equals(tipo_transaccion)){
            return opsCaja;
        }
        
        else if("opAdmin".equals(tipo_transaccion)){
            return opsAdmin;
        }
        else if("otros".equals(tipo_transaccion)){
            return opsOtros;
        }
        else{
            return null;
        }
    }
    
    
        
}
