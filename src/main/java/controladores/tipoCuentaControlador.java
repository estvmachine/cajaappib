/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import modelos.TipoCuenta;

/**
 *
 * @author Esteban
 */
public class tipoCuentaControlador {
    private final DBCollection tipoCuentasCollection;
    private MongoClient cliente;
    private DB database;
    
    
    public tipoCuentaControlador() {
         
        try {
        // Creo una instancia de cliente de MongoDB
            cliente = new MongoClient(new ServerAddress("localhost", 27017));
        } catch (UnknownHostException ex) {
            Logger.getLogger(ex.toString());
        }
          
        //Creo conexion con base de datos
        database = cliente.getDB("AppBasilea");
        
        tipoCuentasCollection = database.getCollection("tipoCuentas");

    }
    
    public boolean add(TipoCuenta tipoCuenta){
        
           //Determino la fecha para guardarla
            Date now = new Date();
            tipoCuenta.append("create_at", now);
            
          // Inserto el documento en la coleccion de cuentaes en el database
        try {
            tipoCuentasCollection.insert(tipoCuenta);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    }
    
    public boolean agregar(String cod, String nombre){
        
        //Determino la fecha para guardarla
        Date now = new Date();

        // Creo un documento para agregar a la coleccion.
        DBObject document = new BasicDBObject("cod", cod)
            .append("nombre", nombre)
            .append("create_at", now);
            
          // Inserto el documento en la coleccion de cuentaes en el database
        try {
            tipoCuentasCollection.insert(document);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    }
    
    public List<DBObject> listar() {
        
       try{
            DBCursor resultados = tipoCuentasCollection.find();
            return resultados.toArray();
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
}
