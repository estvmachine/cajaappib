/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import com.mongodb.DBObject;
import controladores.cuentaControlador;
import controladores.transaccionControlador;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import modelos.Transaccion;

/**
 *
 * @author STEV
 */
public class panelGiroChequeraElectronica_TR0500 extends javax.swing.JPanel  {

    private transaccionControlador transaccionControl;
    private cuentaControlador cuentaControl;
    private DBObject cajero;
    private String diaContable;
    
    
    panelGiroChequeraElectronica_TR0500(DBObject cajero, String diaContable) {
         initComponents();
         this.cajero= cajero;
         this.diaContable= diaContable;
         transaccionControl= new transaccionControlador();
         cuentaControl = new cuentaControlador();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        botonAceptarGiro = new javax.swing.JButton();
        inputMonto = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        inputNumCuenta = new javax.swing.JTextField();
        inputTipoCuenta = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        inputRutTitular = new javax.swing.JTextField();
        Resultados = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        outputRespuesta = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos Giro"));
        jPanel2.setToolTipText("");

        botonAceptarGiro.setText("Aceptar");
        botonAceptarGiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarGiroActionPerformed(evt);
            }
        });

        inputMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                inputMontoKeyTyped(evt);
            }
        });

        jLabel4.setText("Monto              :");

        jLabel3.setText("N.de cuenta    :");

        inputNumCuenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                inputNumCuentaKeyTyped(evt);
            }
        });

        inputTipoCuenta.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cuenta Corriente", "Cuenta RUT"}));

        jLabel2.setText("Tipo de Cuenta:");

        jLabel5.setText("RUT del Titular     :");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonAceptarGiro)
                .addGap(218, 218, 218))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(jLabel4))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel3)))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(89, 89, 89)
                            .addComponent(jLabel2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(inputRutTitular, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(inputMonto)
                            .addComponent(inputNumCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(inputTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(206, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inputTipoCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(inputNumCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(inputRutTitular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inputMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addComponent(botonAceptarGiro))
        );

        Resultados.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados"));
        Resultados.setToolTipText("");
        Resultados.setName("Resultados"); // NOI18N

        outputRespuesta.setEditable(false);
        outputRespuesta.setColumns(20);
        outputRespuesta.setRows(5);
        jScrollPane1.setViewportView(outputRespuesta);

        jLabel7.setText("Respuesta   :");

        javax.swing.GroupLayout ResultadosLayout = new javax.swing.GroupLayout(Resultados);
        Resultados.setLayout(ResultadosLayout);
        ResultadosLayout.setHorizontalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResultadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        ResultadosLayout.setVerticalGroup(
            ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ResultadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ResultadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(0, 80, Short.MAX_VALUE))
        );

        jLabel1.setText("0500 Giro Chequera Electrónica");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Resultados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(197, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(100, 100, 100))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Resultados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 562, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 469, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void botonAceptarGiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarGiroActionPerformed
        // TODO add your handling code here:

        String tipoCuenta= this.inputTipoCuenta.getSelectedItem().toString();
        String numCuenta= this.inputNumCuenta.getText().toString();
        int monto= Integer.parseInt(this.inputMonto.getText().toString());
        String rut_cliente= this.inputRutTitular.getText().toString();
        Transaccion transaccion= new Transaccion("0500");

        DBObject cuenta= cuentaControl.validarCuenta(numCuenta, tipoCuenta);

        //Si la cuenta existe
        if(cuenta != null ){
            //Si el el rut del cliente coincide con el de la cuenta, y el monto es menor a 400.000
            if (cuenta.get("rut_cliente").toString().equals(rut_cliente)  && monto<= 400000){
                transaccion.append("rut_cliente", cuenta.get("rut_cliente"))
                .append("rut_cajero", cajero.get("rut"))
                .append("monto",monto)
                .append("cuenta", numCuenta)
                .append("tipoCuenta",tipoCuenta)
                .append("diaContable", diaContable)
                .append("estado", "Aceptada");

                this.inputRutTitular.setText(cuenta.get("rut_cliente").toString());
                this.outputRespuesta.setText("Transacción ha sido un éxito");
                System.out.println(transaccionControl.agregarTransaccion(transaccion));
                System.out.println(cuentaControl.addMonto(-monto, numCuenta, tipoCuenta));
            }

            //Si el el rut del cliente no coincide con el de la cuenta, y el monto es menor a 400.000
            if (!cuenta.get("rut_cliente").toString().equals(rut_cliente)  && monto<= 400000){
                this.outputRespuesta.setText("Usuario que solicita dinero, no coincide su RUT con el de la cuenta");
                transaccion.append("rut_cliente", rut_cliente )
                .append("rut_cajero", cajero.get("rut"))
                .append("monto",monto)
                .append("cuenta", numCuenta)
                .append("tipoCuenta",tipoCuenta)
                .append("estado", "Cancelada")
                .append("diaContable", diaContable)
                .append("mensaje","Usuario que solicita dinero, no coincide su RUT con el de la cuenta");
                System.out.println(transaccionControl.agregarTransaccion(transaccion));
            }
            
            if( cuenta.get("rut_cliente").toString().equals(rut_cliente)  && monto> 400000){
             //Custom button text
                int resp=JOptionPane.showConfirmDialog(null,"Esta transaccion requiere validacion de supervisor");
                
                if (JOptionPane.OK_OPTION == resp){
                    System.out.println("Selecciona opción Afirmativa");
                }
                else{
                    System.out.println("No selecciona una opción afirmativa");
                }
            }
        }
        //Si la cuenta no existe
        else{
            this.outputRespuesta.setText("Ha habido un error, no se encuentra esta cuenta");
            transaccion.append("rut_cliente", "Error")
            .append("rut_cajero", cajero.get("rut"))
            .append("monto",monto)
            .append("cuenta", numCuenta)
            .append("tipoCuenta",tipoCuenta)
            .append("estado", "Cancelada")
            .append("diaContable", diaContable)
            .append("mensaje","Ha habido un error, no se encuentra esta cuenta");
            System.out.println(transaccionControl.agregarTransaccion(transaccion));
        }
    }//GEN-LAST:event_botonAceptarGiroActionPerformed

    private void inputMontoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputMontoKeyTyped
        // TODO add your handling code here:

        char c = evt.getKeyChar();

        if(c<'0' | c> '9' ) evt.consume();
    }//GEN-LAST:event_inputMontoKeyTyped

    private void inputNumCuentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_inputNumCuentaKeyTyped
        // TODO add your handling code here:

        char c = evt.getKeyChar();

        if(  (c==(char)KeyEvent.VK_SPACE))  evt.consume();
    }//GEN-LAST:event_inputNumCuentaKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Resultados;
    private javax.swing.JButton botonAceptarGiro;
    private javax.swing.JTextField inputMonto;
    private javax.swing.JTextField inputNumCuenta;
    private javax.swing.JTextField inputRutTitular;
    private javax.swing.JComboBox inputTipoCuenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea outputRespuesta;
    // End of variables declaration//GEN-END:variables
}
