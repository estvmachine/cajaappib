/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.QueryBuilder;
import com.mongodb.ServerAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;
import modelos.Transaccion;
import modelos.TipoTransaccion;

public class transaccionControlador {
    
    private static DBCollection transaccionesCollection;
    private MongoClient cliente;
    private DB database;
    
    
   /* private Jongo jongo;
    private MongoCollection JTransacciones;
    private MongoCursor<JTransaccion> cursorJTransacciones;*/
    
    public transaccionControlador() {
          
        try {
        // Creo una instancia de cliente de MongoDB
            cliente = new MongoClient(new ServerAddress("localhost", 27017));
        } catch (UnknownHostException ex) {
            Logger.getLogger(ex.toString());
        }
          
        //Creo conexion con base de datos
        database = cliente.getDB("AppBasilea");
        
        /*
        jongo = new Jongo(database);
        JTransacciones = jongo.getCollection("transacciones");*/
        
        transaccionesCollection = database.getCollection("transacciones");
         
    }
    
    public boolean agregarTransaccion(Transaccion transaccion){
        
        // Inserto el documento en la coleccion de transacciones en el database
        try {
            transaccionesCollection.insert(transaccion);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    }
    
    public boolean agregarTransaccion(String cod_transaccion, String rut_cliente, String rut_cajero, int monto, String cuenta, String estado, Date now){

            //Determino la fecha para guardarla
            if (now != null)
                now = new Date();

            // Creo un documento para agregar a la coleccion.
            DBObject document = new BasicDBObject("cod_transaccion", cod_transaccion)
                    .append("rut_cliente", rut_cliente)
                    .append("rut_cajero", rut_cajero)
                    .append("monto", monto)
                    .append("cuenta", cuenta)
                    .append("estado", estado)
                    .append("create_at", now);

            // Inserto el documento en la coleccion de transacciones en el database
        try {
            transaccionesCollection.insert(document);
            return true;
        } 
            catch (MongoException.DuplicateKey e) {
            System.out.println("Error en el guardado");
            return false;
        }
    

    }
    
      
    public List<DBObject> listar(String usuario, String fecha_contable, String hora_inicial, String hora_final, String estado_transaccion, String tipo_transaccion) {
        
       try{
            System.out.println(estado_transaccion.equals("todos"));
           if(estado_transaccion.equals("todos")){
                DBObject query = new QueryBuilder()
                        .start()
                        .and(
                            new QueryBuilder().start("rut_cajero").is(usuario).get(),
                            new QueryBuilder().start("diaContable").is(fecha_contable).get()
                ).get();
                System.out.println(fecha_contable); 
                System.out.println(query);

                DBCursor resultados = transaccionesCollection.find(query);
                return resultados.toArray();
           }
           else{
                DBObject query = new QueryBuilder()
                        .start()
                        .and(
                            new QueryBuilder().start("rut_cajero").is(usuario).get(),
                            new QueryBuilder().start("diaContable").is(fecha_contable).get(),
                            new QueryBuilder().start("tipo_transaccion").is(tipo_transaccion).get()
                            
                ).get();

                DBCursor resultados = transaccionesCollection.find(query);
                return resultados.toArray();
           }
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
   
    public List<DBObject> addFiltro(List<DBObject> listaFiltros, String key, String value){
        DBObject temp;
        temp= new BasicDBObject(key, value);
        listaFiltros.add(temp);
        return listaFiltros;
    }
    
     public List<DBObject> addFiltro(List<DBObject> listaFiltros, DBObject filtro){
        listaFiltros.add(filtro);
        return listaFiltros;
    }
    
    
    public static List<DBObject> filtrar(List<DBObject> filtros){
        List<DBObject> listParams = new ArrayList<>();
        ListIterator iterator= filtros.listIterator();
        QueryBuilder query= new QueryBuilder();
      
        //Compruebo todos los filtros que lleguen, si son alguno de los que reconozco
        while(iterator.hasNext()){
            DBObject temp = (DBObject) iterator.next();
            if(temp.get("estado") != null )
                listParams.add(QueryBuilder.start("estado").is(temp.get("estado")).get());
            
            else if(temp.get("rut_cajero")!=null){
               DBObject queryPart= QueryBuilder.start("rut_cajero").is(temp.get("rut_cajero")).get();
               listParams.add(queryPart); 
            }
            else if(temp.get("diaContable")!= null){
               DBObject queryPart= QueryBuilder.start("diaContable").is(temp.get("diaContable")).get();
               listParams.add(queryPart); 
            }
            else if(temp.get("tipoTrans")!= null && TipoTransaccion.isAceptableList((String) temp.get("tipoTrans")) ){
                ArrayList<String> listaTiposTrans = TipoTransaccion.getListTransaccionByType((String) temp.get("tipoTrans"));
                DBObject queryPart =  QueryBuilder.start("cod_transaccion").in(listaTiposTrans).get();
                listParams.add(queryPart); 
            }
            
        }
        //Los filtros los agrego como condiciones AND por el momento en el sistema
        DBObject[] arrayParams = new DBObject[listParams.size()];
        arrayParams = listParams.toArray(arrayParams);
        
        query.and(arrayParams);
                
        //Ejecuto la query solicitada
        try{
            System.out.println(query.get());
            DBCursor resultados = transaccionesCollection.find((query.get()));
            return resultados.toArray();
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }    
    }
    
    /*public List<JTransaccion> filtrarJ(){
        List<JTransaccion> lista = null;
        JTransaccion jtransaccion;
        
        cursorJTransacciones= JTransacciones.find().as(JTransaccion.class);
        
        while(cursorJTransacciones.hasNext()){
            jtransaccion= cursorJTransacciones.next();
            System.out.println(jtransaccion._id);
            System.out.println(jtransaccion.diaContable);
            System.out.println(jtransaccion.Modo_Contable);
            System.out.println(jtransaccion.Tipo_Apertura);
            System.out.println(jtransaccion.cod_transaccion);
            System.out.println(jtransaccion.create_at);
            System.out.println(jtransaccion.rut_cajero);
            
        }
        return lista;
    }*/
    
    
    
    
}